﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using QSWebApi.Models;

namespace QSWebApi.Controllers
{
    public class QuizScoresController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/QuizScores?userId=5
        public IQueryable<QuizScore> GetQuizScores(string userId)
        {
            return db.QuizScores.Where(s => s.User_Id == userId);
        }

        // GET: api/QuizScores/5
        [ResponseType(typeof(QuizScore))]
        public IHttpActionResult GetQuizScore(int id)
        {
            QuizScore quizScore = db.QuizScores.Find(id);
            if (quizScore == null)
            {
                return NotFound();
            }

            return Ok(quizScore);
        }

        // PUT: api/QuizScores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutQuizScore(int id, QuizScore quizScore)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != quizScore.Id)
            {
                return BadRequest();
            }

            db.Entry(quizScore).State = EntityState.Modified;

            try
            {
                List<Question> Questions = db.Questions.Where(q => q.Quiz_Id == quizScore.Quiz_Id).ToList();
                List<Answer> Answers = db.Answers.Where(a => a.User_Id == quizScore.User_Id && a.Quiz_Id == quizScore.Quiz_Id).ToList();

                int score = 0;
                bool? choice3, choice4;
                foreach (Answer answer in Answers)
                {

                    Question question = Questions.FirstOrDefault(q => q.Id == answer.Question_Id);

                    if (question.Choice3 == null)
                        choice3 = false;
                    else choice3 = question.IsChoice3Correct;
                    if (question.Choice4 == null)
                        choice4 = false;
                    else choice4 = question.IsChoice4Correct;

                    if (answer.Choice1 == question.IsChoice1Correct && answer.Choice2 == question.IsChoice2Correct &&
                           answer.Choice3 == choice3 && answer.Choice4 == choice4)
                    {
                        score++;
                    }
                }

                quizScore.Score = score;
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuizScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            quizScore.Quiz = null;
            quizScore.AspNetUser = null;

            return Ok(quizScore);
        }

        // POST: api/QuizScores
        [ResponseType(typeof(_QuizScore))]
        public IHttpActionResult PostQuizScore(QuizScore quizScore)
        {
            QuizScore q_s = db.QuizScores.FirstOrDefault(qs => qs.User_Id == quizScore.User_Id && qs.Quiz_Id == quizScore.Quiz_Id);
            if (q_s != null)
            {
                DateTime startDate = q_s.Date;
                int time = db.Quizzes.FirstOrDefault(q => q.Id == quizScore.Quiz_Id).Time;

                if (DateTime.Now > startDate.AddMinutes(time))
                    return StatusCode(HttpStatusCode.Forbidden);
                return Ok(new _QuizScore(q_s));
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.QuizScores.Add(quizScore);

            try
            {
                quizScore.Date = new DateTime();
                quizScore.Date = DateTime.Now;
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (QuizScoreExists(quizScore.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = quizScore.Id }, new _QuizScore(quizScore));
        }

        // DELETE: api/QuizScores/5
        [ResponseType(typeof(QuizScore))]
        public IHttpActionResult DeleteQuizScore(int id)
        {
            QuizScore quizScore = db.QuizScores.Find(id);
            if (quizScore == null)
            {
                return NotFound();
            }

            db.QuizScores.Remove(quizScore);
            db.SaveChanges();

            return Ok(quizScore);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuizScoreExists(int id)
        {
            return db.QuizScores.Count(e => e.Id == id) > 0;
        }
    }
}