﻿using QSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class AvailableQuizzesController : ApiController
    {
        private Models.QSEntities db = new QSEntities();

        // GET: api/Quizzes
        public IEnumerable<_Quiz> GetAvailableQuizzes()
        {
            List<_Quiz> Quizzes = new List<_Quiz>();
            DateTime currentDate = DateTime.Now.Date;

            foreach (Quiz quiz in db.Quizzes.Where(q => q.ValidationDate >= currentDate))
            {
                Quizzes.Add(new _Quiz(quiz));
            }
            return Quizzes;
        }
    }
}
