﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QSWebApi.Models;
using System.Web.Http.Description;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class AspNetUserRoleController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/AspNetUserRole/5
        [ResponseType(typeof(string))]
        public IHttpActionResult GetAspNetUserRole(string userName)
        {
            AspNetUser aspNetUser = db.AspNetUsers.FirstOrDefault(u => u.UserName == userName);
            if (aspNetUser == null)
            {
                return NotFound();
            }

            return Ok(aspNetUser.Role);
        }
    }
}
