﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using QSWebApi.Models;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class QuestionsController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/Questions
        /*
        public IQueryable<Question> GetQuestions()
        {
            return db.Questions;
        }
        */

        // GET: api/Questions/5
        [ResponseType(typeof(QuizQuestion))]
        public IHttpActionResult GetQuestion(int id)
        {
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return NotFound();
            }

            return Ok(new QuizQuestion(question));
        }

        // GET: api/Questions/questionId=5
        [ResponseType(typeof(QuizQuestion))]
        public IHttpActionResult GetQuestionSecured([FromUri]int questionId)
        {
            Question question = db.Questions.Find(questionId);
            if (question == null)
            {
                return NotFound();
            }

            question.IsChoice1Correct = false;
            question.IsChoice2Correct = false;
            question.IsChoice3Correct = false;
            question.IsChoice4Correct = false;

            return Ok(new QuizQuestion(question));
        }

        // PUT: api/Questions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutQuestion(int id, Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.Id)
            {
                return BadRequest();
            }

            db.Entry(question).State = EntityState.Modified;

            try
            {
                Quiz quiz = db.Quizzes.FirstOrDefault(q => q.Id == question.Quiz_Id);
                quiz.NumberOfQuestions = (byte)(db.Questions.Where(q => q.Quiz_Id == question.Quiz_Id).Count() + 1);
                db.SaveChanges();

                question.Quiz = null;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Questions
        [ResponseType(typeof(Question))]
        public IHttpActionResult PostQuestion(Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Questions.Add(question);
            Quiz quiz = db.Quizzes.FirstOrDefault(q => q.Id == question.Quiz_Id);
            quiz.NumberOfQuestions = (byte)(db.Questions.Where(q => q.Quiz_Id == question.Quiz_Id).Count() + 1);
            db.SaveChanges();

            question.Quiz = null;

            return CreatedAtRoute("DefaultApi", new { id = question.Id }, question);
        }

        // DELETE: api/Questions/5
        [ResponseType(typeof(Question))]
        public IHttpActionResult DeleteQuestion(int id)
        {
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return NotFound();
            }

            db.Questions.Remove(question);
            Quiz quiz = db.Quizzes.FirstOrDefault(q => q.Id == question.Quiz_Id);
            quiz.NumberOfQuestions = (byte)(db.Questions.Where(q => q.Quiz_Id == question.Quiz_Id).Count() + 1);
            db.SaveChanges();

            question.Quiz = null;

            return Ok(question);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuestionExists(int id)
        {
            return db.Questions.Count(e => e.Id == id) > 0;
        }
    }
}