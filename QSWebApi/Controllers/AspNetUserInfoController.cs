﻿using QSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class AspNetUserInfoController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/AspNetUserInfo/UserName=""
        [ResponseType(typeof(UserInfo))]
        public IHttpActionResult GetAspNetUserRole(string userName)
        {
            AspNetUser aspNetUser = db.AspNetUsers.FirstOrDefault(u => u.UserName == userName);
            if (aspNetUser == null)
            {
                return NotFound();
            }

            UserInfo userInfo = new UserInfo(aspNetUser);
            return Ok(userInfo);
        }
    }
}
