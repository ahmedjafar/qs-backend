﻿using QSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class QuestionIdsController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/QuestionIds/5
        [ResponseType(typeof(List<string>))]
        public IHttpActionResult GetQuizQuestionIds(int id)
        {
            Quiz quiz = db.Quizzes.FirstOrDefault(q => q.Id == id);
            if(quiz == null)
                return StatusCode(HttpStatusCode.NotFound);
            var ids = db.Questions.Where(q => q.Quiz_Id == id).Select(result => new { id = result.Id });
            return Ok(ids);
        }
    }
}
