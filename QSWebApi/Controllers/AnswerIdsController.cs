﻿using QSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class AnswerIdsController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/QuizIds/
        [ResponseType(typeof(List<AnswerId>))]
        public IHttpActionResult GetAnswerIds([FromUri] string userId, [FromUri] int quizId)
        {
            AspNetUser user = db.AspNetUsers.FirstOrDefault(u => u.Id == userId && u.Role=="Testee");
            if(user == null)
                return StatusCode(HttpStatusCode.NotFound);

            Quiz quiz = db.Quizzes.FirstOrDefault(q => q.Id == quizId);
            if (quiz == null)
                return StatusCode(HttpStatusCode.NotFound);

            var ids = db.Answers.Where(a => a.Quiz_Id == quizId && a.User_Id == userId).Select(result => new { Question_Id = result.Question_Id, Answer_Id = result.Id });
            return Ok(ids);
        }
    }
}
