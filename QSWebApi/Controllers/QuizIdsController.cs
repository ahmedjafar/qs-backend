﻿using QSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class QuizIdsController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/QuizIds/
        [ResponseType(typeof(string))]
        public IQueryable GetQuizIds()
        {
            var ids = db.Quizzes.Select(q => new { id = q.Id });
            return ids;
        }
    }
}
