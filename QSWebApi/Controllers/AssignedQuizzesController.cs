﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using QSWebApi.Models;

namespace QSWebApi.Controllers
{
    public class AssignedQuizzesController : ApiController
    {
        private QSEntities db = new QSEntities();

        // GET: api/AssignedQuizzes
        public IQueryable<AssignedQuiz> GetAssignedQuizs()
        {
            return db.AssignedQuizs;
        }

        // GET: api/AssignedQuizzes/5
        [ResponseType(typeof(AssignedQuiz))]
        public IHttpActionResult GetAssignedQuiz(int id)
        {
            AssignedQuiz assignedQuiz = db.AssignedQuizs.Find(id);
            if (assignedQuiz == null)
            {
                return NotFound();
            }

            return Ok(assignedQuiz);
        }

        // PUT: api/AssignedQuizzes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssignedQuiz(int id, AssignedQuiz assignedQuiz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assignedQuiz.Id)
            {
                return BadRequest();
            }

            db.Entry(assignedQuiz).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssignedQuizExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AssignedQuizzes
        [ResponseType(typeof(AssignedQuiz))]
        public IHttpActionResult PostAssignedQuiz(AssignedQuiz assignedQuiz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AssignedQuizs.Add(assignedQuiz);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = assignedQuiz.Id }, assignedQuiz);
        }

        // DELETE: api/AssignedQuizzes/5
        [ResponseType(typeof(AssignedQuiz))]
        public IHttpActionResult DeleteAssignedQuiz(int id)
        {
            AssignedQuiz assignedQuiz = db.AssignedQuizs.Find(id);
            if (assignedQuiz == null)
            {
                return NotFound();
            }

            db.AssignedQuizs.Remove(assignedQuiz);
            db.SaveChanges();

            return Ok(assignedQuiz);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssignedQuizExists(int id)
        {
            return db.AssignedQuizs.Count(e => e.Id == id) > 0;
        }
    }
}