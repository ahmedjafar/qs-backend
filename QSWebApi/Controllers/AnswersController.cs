﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using QSWebApi.Models;

namespace QSWebApi.Controllers
{
    [Authorize]
    public class AnswersController : ApiController
    {
        private QSEntities db = new QSEntities();

        /*
        // GET: api/Answers
        public IQueryable<Answer> GetAnswers()
        {
            return db.Answers;
        }
        */

        // GET: api/Answers/5
        [ResponseType(typeof(_Answer))]
        public IHttpActionResult GetAnswer(int id)
        {
            Answer answer = db.Answers.FirstOrDefault(a => a.Id == id);
            if (answer == null)
            {
                return NotFound();
            }

            return Ok(new _Answer(answer));
        }

        // PUT: api/Answers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnswer(int id, Answer answer)
        {
            int time = db.Quizzes.FirstOrDefault(q => q.Id == answer.Quiz_Id).Time;
            DateTime startDate = db.QuizScores.FirstOrDefault(qs => qs.User_Id == answer.User_Id && qs.Quiz_Id == answer.Quiz_Id).Date;
            if (DateTime.Now > startDate.AddMinutes(time))
                return StatusCode(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != answer.Id)
            {
                return BadRequest();
            }

            db.Entry(answer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnswerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Answers
        [ResponseType(typeof(_Answer))]
        public IHttpActionResult PostAnswer(Answer answer)
        {
            int time = db.Quizzes.FirstOrDefault(q => q.Id == answer.Quiz_Id).Time;
            DateTime startDate = db.QuizScores.FirstOrDefault(qs => qs.User_Id == answer.User_Id && qs.Quiz_Id == answer.Quiz_Id).Date;
            if (DateTime.Now > startDate.AddMinutes(time))
                return StatusCode(HttpStatusCode.Forbidden);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Answers.Add(answer);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AnswerExists(answer.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = answer.Id }, new _Answer(answer));
        }

        // DELETE: api/Answers/5
        [ResponseType(typeof(Answer))]
        public IHttpActionResult DeleteAnswer(int id)
        {
            Answer answer = db.Answers.Find(id);
            if (answer == null)
            {
                return NotFound();
            }

            db.Answers.Remove(answer);
            db.SaveChanges();

            return Ok(answer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnswerExists(int id)
        {
            return db.Answers.Count(e => e.Id == id) > 0;
        }
    }
}