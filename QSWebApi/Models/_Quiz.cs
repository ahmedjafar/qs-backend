﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class _Quiz
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ValidationDate { get; set; }
        public int? NumberOfQuestions { get; set; }
        public int Time { get; set; }

        public _Quiz()
        {

        }
        public _Quiz(Quiz quiz)
        {
            Id = quiz.Id;
            Name = quiz.Name;
            CreationDate = quiz.CreationDate;
            ValidationDate = quiz.ValidationDate;
            NumberOfQuestions = quiz.NumberOfQuestions;
            Time = quiz.Time;
        }
    }
}