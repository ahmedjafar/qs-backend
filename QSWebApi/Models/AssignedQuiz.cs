//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QSWebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AssignedQuiz
    {
        public int Id { get; set; }
        public int Quiz_Id { get; set; }
        public string User_Id { get; set; }
        public System.DateTime DueDate { get; set; }
        public Nullable<int> Score { get; set; }
        public Nullable<System.DateTime> AttemptDate { get; set; }
        public bool IsCanceled { get; set; }
    
        public virtual Quiz Quiz { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
