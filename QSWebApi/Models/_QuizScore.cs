﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class _QuizScore
    {
        public int Id { get; set; }
        public int Quiz_Id { get; set; }
        public string User_Id { get; set; }
        public int? Score { get; set; }
        public DateTime Date { get; set; }

        public _QuizScore(QuizScore score)
        {
            Id = score.Id;
            Quiz_Id = score.Quiz_Id;
            User_Id = score.User_Id;
            Score = score.Score;
            Date = score.Date;
        }
    }
}