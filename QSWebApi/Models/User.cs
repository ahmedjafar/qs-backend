﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class User
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public User()
        {

        }

        public User(AspNetUser user)
        {
            Id = user.Id;
            UserName = user.UserName;
            Email = user.Email;
            Password = user.PasswordHash;
        }
    }
}