﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class UserInfo
    {
        public string UserId { get; set; }
        public string Role { get; set; }

        public UserInfo(AspNetUser user)
        {
            UserId = user.Id;
            Role = user.Role;
        }
    }
}