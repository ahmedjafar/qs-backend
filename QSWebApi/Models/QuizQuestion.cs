﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class QuizQuestion
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        public string Choice1 { get; set; }
        public string Choice2 { get; set; }
        public string Choice3 { get; set; }
        public string Choice4 { get; set; }
        public bool IsChoice1Correct { get; set; }
        public bool IsChoice2Correct { get; set; }
        public bool? IsChoice3Correct { get; set; }
        public bool? IsChoice4Correct { get; set; }
        public int Quiz_Id { get; set; }

        public QuizQuestion(Question question)
        {
            Id = question.Id;
            QuestionText = question.QuestionText;
            Choice1 = question.Choice1;
            Choice2 = question.Choice2;
            Choice3 = question.Choice3;
            Choice4 = question.Choice4;
            IsChoice1Correct = question.IsChoice1Correct;
            IsChoice2Correct = question.IsChoice2Correct;
            IsChoice3Correct = question.IsChoice3Correct;
            IsChoice4Correct = question.IsChoice4Correct;
            Quiz_Id = question.Quiz_Id;
        }
    }
}