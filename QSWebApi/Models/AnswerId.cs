﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class AnswerId
    {
        public string Question_Id { get; set; }
        public string Answer_Id { get; set; }
    }
}