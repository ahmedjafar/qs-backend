﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QSWebApi.Models
{
    public class _Answer
    {
        public int Id { get; set; }
        public int Quiz_Id { get; set; }
        public string User_Id { get; set; }
        public int Question_Id { get; set; }
        public bool Choice1 { get; set; }
        public bool Choice2 { get; set; }
        public bool Choice3 { get; set; }
        public bool Choice4 { get; set; }

        public _Answer(Answer answer)
        {
            Id = answer.Id;
            Quiz_Id = answer.Quiz_Id;
            User_Id = answer.User_Id;
            Question_Id = answer.Question_Id;
            Choice1 = answer.Choice1;
            Choice2 = answer.Choice2;
            Choice3 = answer.Choice3;
            Choice4 = answer.Choice4;
        }
    }
}